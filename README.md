# LimeSurvey SkelVanilla Survey Theme

Work in Progress, not ready for productuon

## About
A clean and simple base that can be used by developers to create their own Bootstrap based theme.

### Feature

- Usage of aria-describedby for help, mandatory and dynamic help
- Keyboard navigation and awesome radio/checkbox
- No information for screen reader about HTML updated
- Better dark mode support
- Page role and shorcut link
- Keyboard on language and index menu 